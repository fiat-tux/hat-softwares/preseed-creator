# Preseed creator

Small script aimed to ease the creation of a preseeded Debian ISO image

## Installation

### Debian package

Go to <https://framagit.org/fiat-tux/hat-softwares/preseed-creator/-/releases/permalink/latest>, download the Debian package and its signature file.

Check the signature of the Debian package and install the package:
```bash
minisign -Vm preseed-creator_*.deb -P RWQm7sZSguNVtitOiU4ozG+HgWWyUz4KmB0NMhjuTYkMardZlXjsKWLk &&
sudo apt install ./preseed-creator_*.deb
```

You may want to install `xorriso` if you want to create hybrid ISO files.

### Manual installation:

In order to run the script, you need to install these packages:

```
apt install wget genisoimage rsync cpio
```

You may want to install `xorriso` if you want to create hybrid ISO files.

Then:
```bash
wget https://framagit.org/fiat-tux/hat-softwares/preseed-creator/-/raw/main/preseed-creator
chmod +x preseed-creator
```

Then put the script where you want (like in your $PATH)

## How to use

The script needs to be run as root.

`preseed-creator -h` will teach you how to use it.

If you installed the Debian package, you can use `man preseed-creator`

## License

Released under the terms of the GNU GPLv3. See [LICENSE](LICENSE) file.

## Author

Luc Didry, <https://fiat-tux.fr>

## Contributor

[@adriengg](https://framagit.org/adriengg) for the hybrid iso part
